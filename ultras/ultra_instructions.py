from enum import IntEnum

from I2C import I2CInstructions


class UltraInstructions(IntEnum):
    GET_STATE = I2CInstructions.GET_STATE
    READ = 0x03

