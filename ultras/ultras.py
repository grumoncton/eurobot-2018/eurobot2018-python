import os
import math
import numpy as np

from utils import index_of_min_value
from .ultra_instructions import UltraInstructions
from pathfinder.table import Circle

NUM_ULTRAS = 8


class Ultras:
    def __init__(self, i2c, addr):
        self.i2c = i2c
        self.addr = addr
        self.tolerance = int(os.getenv('ULTRA_TOLERANCE'))
        self.distances = None

    def read_distances(self):
        self.i2c.write(self.addr, UltraInstructions.READ)
        distances = self.i2c.read_block(self.addr, UltraInstructions.READ)
        return 4 * np.array(distances[:NUM_ULTRAS])

    def is_blocked(self):
        self.distances = self.read_distances()
        return any(dist < self.tolerance and dist > 0 for dist in self.distances)

    def generate_obstacle(self, pos):
        index, dist = index_of_min_value(self.read_distances())
        relative_angle = index * math.tau / NUM_ULTRAS
        absolute_angle = relative_angle - pos.a

        return Circle(
            x=pos.x + dist * math.cos(absolute_angle),
            y=pos.y + dist * math.sin(absolute_angle),
            r=int(os.getenv('ULTRA_AVOIDANCE_RADIUS')),
        )

