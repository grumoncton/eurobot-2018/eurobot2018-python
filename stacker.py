from colour import Colour
from coord import Coord
from devices.sucker import Pumps
from devices.wheel_steps import Direction
from task_manager.step import StepGenerator
from table_side import TableSide

_sequence = None
DIST_BLOCKS_ROBOT = 215
DIST_BETWEEN_BLOCKS = 58


def sequence_callback(sequence):
    global _sequence
    _sequence = sequence


def stacker_init(MoveToRelativeStep, pickupHeightStep, DropHeightStep, OverHeightStep, spinnerCWStep, spinnerCCWStep, SuckerStartStep, SuckerStopStep, table_side):

    class Stacker(StepGenerator):
        def __init__(self, task, block_rotation):
            self.task = task
            self.block_rotation = block_rotation

        def is_ready(self):
            return True

        def generate_steps(self):
            global _sequence

            #
            #  Get a colour-pump association
            #

            pump_colour_map = {}
            pump_layout = {
                0: Pumps.BACK,
                90: Pumps.LEFT,
                180: Pumps.FRONT,
                270: Pumps.RIGHT,
            }

            if table_side is TableSide.ORANGE:
                pump_layout[90] = Pumps.RIGHT
                pump_layout[270] = Pumps.LEFT

            #  Center pump is always yellow
            pump_colour_map[Colour.YELLOW] = Pumps.CENTER

            #  Figure out rest of pumps based on rotation
            for i, colour in enumerate(Colour):
                if colour is Colour.YELLOW:
                    continue

                pump = pump_layout[(self.block_rotation + (90 * i)) % 360]
                pump_colour_map[colour] = pump

            #
            #  Figure out order of stack
            #

            #  Default to pumps in ideal order
            order = list(Pumps)

            #  If we have a sequence, put it first
            if _sequence is not None:

                #  If we stack in the right sequence, we get 30 more points
                self.task.score += 30

                #  Get can't place the back one first cause the wall is in the way
                #  If it's first, flip the order
                if pump_colour_map[_sequence[0]] is Pumps.BACK:
                    _sequence = _sequence[::-1]

                order = []

                #  Add corresponding pump for each colour in sequence
                for colour in _sequence:
                    order.append(pump_colour_map[colour])

                #  Add remaining pumps
                for pump in Pumps:
                    if pump in order:
                        continue

                    order.append(pump)

            print('order', order)

            #
            #  Calculate steps
            #

            steps = []
            current_angle = 0
            current_position = 1
            stopped_pumps = []

            #  Loops through order
            for i, pump in enumerate(order):
                level = i + 1

                if level in (2, 3, 4):

                    #  Move lifter to correct drop height
                    steps.append(OverHeightStep(position=level))

                elif level == 5:
                    steps.append(OverHeightStep(position=(level - 1)))

                #  For center pump, we don't care about the angle
                #  just move to the position
                if pump is Pumps.CENTER:
                    steps += self.__move_to(current_position, 0)
                    current_position = 0

                #  For back pump, rotate to center and move backwards
                elif pump is Pumps.BACK:
                    steps += \
                        self.__rotate_to(current_angle, 0, level) + \
                        self.__move_to(current_position, -1)

                    current_angle = 0
                    current_position = -1

                #  For other pumps, move to required angle and move forwards
                else:
                    desired_angle = list(Pumps).index(pump) - 1

                    steps += \
                        self.__rotate_to(current_angle, desired_angle, level) + \
                        self.__move_to(current_position, 1)

                    current_angle = desired_angle
                    current_position = 1

                steps.append(DropHeightStep(position=level))

                stopped_pumps.append(pump)
                steps.append(SuckerStopStep(stopped_pumps))

            steps.append(OverHeightStep(position=5))

            for i, step in enumerate(steps):
                if step is None:
                    del steps[i]

                if i == 0:
                    continue

                if not isinstance(step, MoveToRelativeStep):
                    continue

                prev_step = steps[i - 1]

                if not isinstance(prev_step, MoveToRelativeStep):
                    continue

                steps.pop(i)

                steps[i - 1] = MoveToRelativeStep(prev_step.destination + step.destination, step.direction)

            print()
            for step in steps:
                print(step)
            print()

            return steps

        def __move_to(self, current_position, desired_position):

            #  If we're alreay there, return nothing
            if desired_position == current_position:
                return []

            #  Calculate movement between blocks
            relative_move = Coord(x=58, y=0)

            delta = current_position - desired_position

            direction = Direction.FORWARDS if delta > 0 else Direction.BACKWARDS

            destination = delta * relative_move

            #  if desired_position == 0:
                #  destination += Coord(x=0, y=-58)

            return [MoveToRelativeStep(destination, direction)]

        def __rotate_to(self, current_angle, desired_angle, level):
            if current_angle == desired_angle:
                return []

            delta = desired_angle - current_angle

            step = spinnerCWStep if delta > 0 else spinnerCCWStep

            result = [step] * abs(delta)

            if level == 5:
                result = [MoveToRelativeStep(Coord(x=-100, y=0), Direction.BACKWARDS)] + \
                    result + \
                    [OverHeightStep(level), MoveToRelativeStep(Coord(x=100, y=0))]

            return result

    return Stacker

