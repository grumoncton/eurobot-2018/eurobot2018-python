from enum import IntEnum


class Colour(IntEnum):
    YELLOW = 0
    ORANGE = 1
    BLUE = 2
    GREEN = 3
    BLACK = 4

