import sys
import time
import operator


def timeit(message):

    #  Print without new line
    sys.stdout.write(message)

    start_time = time.time()

    def end():
        end_time = time.time()
        print(' took {} seconds.'.format(round(end_time - start_time, 2)))

    return end


#  Get the index of the smallest value of a list
def index_of_min_value(list):
    return min(enumerate(list), key=operator.itemgetter(1))

