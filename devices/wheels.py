import os
from enum import IntEnum
import numpy as np

from .device import Device
from .states import DeviceStates
from direction import Direction
from coord import Coord, CoordHeading
from I2C import I2CInstructions
from table_side import TableSide

NUM_ULTRAS = 8
ULTRA_TOLERANCE = 300
ULTRA_EDGE_TOLERANCE = 700
ULTRA_ANGLE_TOLERANCE = 60


def constrained_by(x, a, b):
    return a < x and x < b


class WheelInstructions(IntEnum):
    GET_STATE = I2CInstructions.GET_STATE
    SET_SIDE = I2CInstructions.SET_SIDE
    STOP = 0x10
    GET_POS = 0x11
    MOVE_TO = 0x12
    ROTATE_TO = 0x13
    MOVE_TO_PATH = 0x14
    MOVE_TO_RELATIVE = 0x15
    RESET_X = 0x20
    RESET_Y = 0x21
    RESET_A = 0x22


class WheelStates(IntEnum):
    UNINITIATED = DeviceStates.UNINITIATED
    READY = DeviceStates.READY
    WORKING = DeviceStates.WORKING
    STOPPED = DeviceStates.STOPPED
    DONE = DeviceStates.DONE
    DONE_PATH_SEGMENT = 0x10


class Wheels(Device):
    def __init__(self, i2c, addr):
        super().__init__(i2c, addr, WheelStates)

        self.state = WheelStates.UNINITIATED
        self.pos = None
        self.current_direction = Direction.FORWARDS

    def update_position(self):
        res = self.i2c.read_block(self.addr, WheelInstructions.GET_POS)
        self.pos = CoordHeading.from_bytes(res)
        print('Updated position. Got {}'.format(self.pos))

    def stop(self):
        self.i2c.write(self.addr, WheelInstructions.STOP)

    def should_stop(self, distances, table_side):
        print("Checking should stop", distances)

        #  If we're not close to a wall, don't ignore anything
        #  (return true right away)
        #  if self.pos.x > ULTRA_EDGE_TOLERANCE \
           #  and self.pos.y > ULTRA_EDGE_TOLERANCE \
           #  and self.pos.x < 2000 - ULTRA_EDGE_TOLERANCE \
           #  and self.pos.y < 3000 - ULTRA_EDGE_TOLERANCE:
            #  return True

        #  Check each ultra
        for i, dist in enumerate(distances):

            #  Ignore useless side ultras
            if i == 2 or i == 6:
                continue

            if self.current_direction == Direction.BACKWARDS \
               and (i == 7 and i == 0 and i == 1):
                continue

            if self.current_direction == Direction.FORWARDS \
               and (i == 3 or i == 4 or i == 5):
                continue

            #  If ultra isn't blocked, skip it
            if dist > ULTRA_TOLERANCE or dist == 0:
                continue

            blocked_ultra_angle = i * 360 / NUM_ULTRAS * \
                (1 if table_side is TableSide.GREEN else -1)

            obstacle_angle = (self.pos.a + blocked_ultra_angle) % 360

            #  Check if ultra should be ignored because it's close to an edge and the angle it's blocked at is pointing at the wall
            if self.pos.x < ULTRA_EDGE_TOLERANCE \
               and constrained_by(obstacle_angle, 180 - ULTRA_ANGLE_TOLERANCE, 180 + ULTRA_ANGLE_TOLERANCE):

                #  Serial.println((String) i + " ultra ignored by x min wall");
                print('{} ignored by x min wall'.format(i))
                continue

            if self.pos.y < ULTRA_EDGE_TOLERANCE \
               and constrained_by(obstacle_angle, 270 - ULTRA_ANGLE_TOLERANCE, 270 + ULTRA_ANGLE_TOLERANCE):

                #  Serial.println((String) i + " ultra ignored by y min wall");
                print('{} ignored by y min wall'.format(i))
                print(obstacle_angle)
                continue

            if self.pos.x > (2000 - ULTRA_EDGE_TOLERANCE) \
               and constrained_by(obstacle_angle, 360 - ULTRA_ANGLE_TOLERANCE, 0 + ULTRA_ANGLE_TOLERANCE):

                #  Serial.println((String) i + " ultra ignored by x max wall");
                print('{} ignored by x max wall'.format(i))
                continue

            if self.pos.y > (3000 - ULTRA_EDGE_TOLERANCE) \
               and constrained_by(obstacle_angle, 90 - ULTRA_ANGLE_TOLERANCE, 90 + ULTRA_ANGLE_TOLERANCE):

                #  Serial.println((String) i + " ultra ignored by y max wall");
                print('{} ignored by y max wall'.format(i))
                continue

            #  Serial.println((String) i + " fucked it up for everybody. Dist: " + (String) dist);

            print("Fuck have to stop", i, self.current_direction)
            return True

        #  If we get through all the ultras and all of them are not blocked or should be ignored, return false
        print("Nah all g")
        return False

