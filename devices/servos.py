from enum import IntEnum

from I2C import I2CInstructions
from .device import Device
from .states import DeviceStates
from task_manager.step import Step


class ServosInstructions(IntEnum):
    GET_STATE = I2CInstructions.GET_STATE
    SET_SIDE = I2CInstructions.SET_SIDE
    CLOSE_LATCH = 0x10
    OPEN_LATCH = 0x11
    CLOSE_ARM = 0x20
    OPEN_ARM = 0x21
    SORT_BALLS = 0x40
    UNCLOG_THAT_ASS = 0x50


class ServosStates(IntEnum):
    UNINITIATED = DeviceStates.UNINITIATED
    READY = DeviceStates.READY
    WORKING = DeviceStates.WORKING
    DONE = DeviceStates.DONE


class Servos(Device):
    def __init__(self, i2c, addr):
        super().__init__(i2c, addr, ServosStates)

        self.state = ServosStates.UNINITIATED


def servos_init(i2c, servos):
    close_latch_step = Step(servos.addr, servos, ServosInstructions.CLOSE_LATCH)
    open_latch_step = Step(servos.addr, servos, ServosInstructions.OPEN_LATCH)
    close_arm_step = Step(servos.addr, servos, ServosInstructions.CLOSE_ARM)
    open_arm_step = Step(servos.addr, servos, ServosInstructions.OPEN_ARM)
    sort_balls_step = Step(servos.addr, servos, ServosInstructions.SORT_BALLS)
    unclog_that_ass_step = Step(servos.addr, servos, ServosInstructions.UNCLOG_THAT_ASS)

    return close_latch_step, open_latch_step, close_arm_step, open_arm_step, sort_balls_step, unclog_that_ass_step

