from enum import IntEnum
from I2C import I2CInstructions

from task_manager.step import Step
from .device import Device
from .states import DeviceStates


class SpinnerInstructions(IntEnum):
    GET_STATE = I2CInstructions.GET_STATE
    SET_SIDE = I2CInstructions.SET_SIDE
    SERVO_ENABLE = 0x10
    SERVO_DISABLE = 0x11
    STEPPER_CW = 0x12
    STEPPER_CCW = 0x13
    SERVO_DOWN = 0x14


class SpinnerStates(IntEnum):
    UNINITIATED = DeviceStates.UNINITIATED
    READY = DeviceStates.READY
    WORKING = DeviceStates.WORKING
    DONE = DeviceStates.DONE


class Spinner(Device):
    def __init__(self, i2c, addr):
        super().__init__(i2c, addr, SpinnerStates)

        self.state = SpinnerStates.UNINITIATED


def spinner_init(i2c, spinner):

    return \
        Step(spinner.addr, spinner, SpinnerInstructions.SERVO_ENABLE), \
        Step(spinner.addr, spinner, SpinnerInstructions.SERVO_DISABLE), \
        Step(spinner.addr, spinner, SpinnerInstructions.STEPPER_CW), \
        Step(spinner.addr, spinner, SpinnerInstructions.STEPPER_CCW), \
        Step(spinner.addr, spinner, SpinnerInstructions.SERVO_DOWN)

