import math
from itertools import chain

from task_manager.step import Step
from .wheels import WheelStates as States, WheelInstructions as Instructions
from I2C import addrs
from coord import Coord
from direction import Direction


MAX_PATH_LENGTH = 7


def wheel_init(i2c, wheels, pathfinder):

    class ResetStep(Step):
        def __init__(self, values, instruction):
            super().__init__(
                addr=addrs['wheels'],
                device=wheels,
                instruction=instruction,
            )
            self.values = values

        def send(self):
            self.device.i2c.write_block(
                self.addr,
                self.instruction,
                self.values,
            )

    class ResetXStep(ResetStep):
        def __init__(self, x):
            values = Coord(x=x, y=0).to_bytes()[0:2]
            super().__init__(values, instruction=Instructions.RESET_X)

    class ResetYStep(ResetStep):
        def __init__(self, y):
            values = Coord(x=0, y=y).to_bytes()[2:4]
            super().__init__(values, instruction=Instructions.RESET_Y)

    class ResetAStep(ResetStep):
        def __init__(self, a):
            values = [int((a % 360) * 255 / 360)]
            print(values)
            super().__init__(values, instruction=Instructions.RESET_A)

    class MoveToWithoutUltraStep(Step):
        def __str__(self):
            return 'move_to({}, {})'.format(self.destination, self.direction)

        def __init__(self, destination, direction=Direction.FORWARDS, long_way=False):
            super().__init__(
                addr=addrs['wheels'],
                device=wheels,
                instruction=Instructions.MOVE_TO
            )
            self.destination = destination
            self.direction = direction
            self.device.current_direction = direction
            self.long_way = long_way

        def send(self):

            #  Convert destionation to list of bytes
            dest = self.destination.to_bytes()

            bools = (self.long_way << 1) | self.direction.value

            i2c.write_block(
                self.addr,
                self.instruction.value,
                [bools] + dest
            )

    class MoveToStep(MoveToWithoutUltraStep):
        pass

    class MoveToRelativeStep(MoveToStep):
        def __init__(self, destination, direction=Direction.FORWARDS, long_way=False):
            super().__init__(destination, direction, long_way)
            self.instruction = Instructions.MOVE_TO_RELATIVE

        def send(self):

            #  Convert destionation to list of bytes
            dest = self.destination.to_bytes(signed=True)

            print(self.destination, self.direction.name)

            i2c.write_block(
                self.addr,
                self.instruction.value,
                [self.direction.value % 256] + dest
            )

    class RotateToStep(Step):
        def __init__(self, angle, long_way):
            super().__init__(i2c, wheels, Instructions.ROTATE_TO)
            self.addr = addrs['wheels']
            self.device = wheels
            self.angle = angle
            self.long_way = long_way

        def send(self):
            i2c.write_block(
                self.addr,
                Instructions.ROTATE_TO,
                [self.long_way << 1, int((self.angle % 360) * 255 / 360)],
            )

    class MoveToPathStep(Step):
        def __init__(self, destination):
            super().__init__(i2c, wheels, Instructions.MOVE_TO_PATH)
            self.addr = addrs['wheels']
            self.device = wheels
            self.destination = destination
            self.remaining_path = None
            self.device.current_direction = Direction.FORWARDS

        def send(self):
            self.device.update_position()
            path = pathfinder.shortest_path(
                self.device.pos,
                self.destination
            )

            if len(path) > MAX_PATH_LENGTH:
                self.remaining_path = path[MAX_PATH_LENGTH:]
                path = path[:MAX_PATH_LENGTH]

            for point in path:
                print(point)

            self.__send_path(path)

        def loop(self):
            if self.device.state != States.DONE_PATH_SEGMENT:
                return

            print("Looping and done path segment!")

            if self.remaining_path is None:
                print("No more path")
                self.device.state = States.DONE
                return

            self.device.state = States.WORKING

            path = self.remaining_path

            if len(path) > MAX_PATH_LENGTH:
                self.remaining_path = path[MAX_PATH_LENGTH:]
                path = path[:MAX_PATH_LENGTH]
            else:
                self.remaining_path = None

            print("Doing more path")
            for point in path:
                print(point)

            self.__send_path(path)

        def __send_path(self, path):
            i2c.write_block(
                self.addr,
                Instructions.MOVE_TO_PATH,
                chain.from_iterable([
                    coord.to_bytes()[:Coord.SIZE] for coord in path
                ]),
            )

    return MoveToStep, MoveToRelativeStep, MoveToPathStep, RotateToStep, MoveToWithoutUltraStep, ResetXStep, ResetYStep, ResetAStep

