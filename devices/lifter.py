from enum import IntEnum

from I2C import I2CInstructions
from task_manager.step import Step
from .device import Device
from .states import DeviceStates


class LifterInstructions(IntEnum):
    GET_STATE = I2CInstructions.GET_STATE
    SET_SIDE = I2CInstructions.SET_SIDE
    PICKUP = 0x10
    SWITCH = 0x11
    DROP = 0x20
    OVER = 0x30


class LifterStates(IntEnum):
    UNINITIATED = DeviceStates.UNINITIATED
    READY = DeviceStates.READY
    WORKING = DeviceStates.WORKING
    DONE = DeviceStates.DONE


class Lifter(Device):
    def __init__(self, i2c, addr):
        super().__init__(i2c, addr, LifterStates)

        self.state = LifterStates.UNINITIATED

    def update_state(self):
        self.i2c.write(self.addr, I2CInstructions.GET_STATE)
        self.state = self.States(self.i2c.read(self.addr) % 128)
        return self.state


def lifter_init(i2c, lifter):

    class LifterStep(Step):
        def __init__(self, position, instruction):
            super().__init__(i2c, lifter, instruction)
            self.addr = lifter.addr
            self.device = lifter
            self.position = position

        def send(self):
            i2c.write(
                self.addr,
                self.instruction.value | self.position,
            )

    class DropHeightStep(LifterStep):
        def __str__(self):
            return 'drop_height({})'.format(self.position)

        def __init__(self, position):
            super().__init__(position, LifterInstructions.DROP)

    class OverHeightStep(LifterStep):
        def __str__(self):
            return 'over_height({})'.format(self.position)

        def __init__(self, position):
            super().__init__(position, LifterInstructions.OVER)

    pickup_height_step = Step(lifter.addr, lifter, LifterInstructions.PICKUP)

    switch_height_step = Step(lifter.addr, lifter, LifterInstructions.SWITCH)

    return pickup_height_step, switch_height_step, DropHeightStep, OverHeightStep

