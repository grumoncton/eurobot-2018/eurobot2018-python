import time
from enum import IntEnum

from .device import Device
from task_manager.step import Step
from device_states import DeviceStates


class Pumps(IntEnum):
    LEFT = 0
    FRONT = 3
    RIGHT = 4
    CENTER = 1
    BACK = 2


class Sucker(Device):
    def __init__(self, i2c, addr):
        super().__init__(i2c, addr, DeviceStates)
        self.done = False

    def is_done(self):
        return self.done

    def is_ready(self):
        return True


def pump_instruction(pumps):
    result = 0
    for pump in pumps:
        result |= 1 << pump

    return result


def sucker_init(i2c, sucker):

    class SuckerStep(Step):
        def __str__(self):
            return 'sucker({})'.format(bin(self.instruction))

        def __init__(self, instruction):
            super().__init__(i2c, sucker, instruction)
            self.addr = sucker.addr

        def update_device_state(self):
            return DeviceStates.READY

        def send(self):
            super().send()
            time.sleep(0.1)
            super().send()
            self.device.done = True

    class SuckerStartStep(SuckerStep):
        def __init__(self, pumps):
            super().__init__(255 ^ pump_instruction(pumps))

    class SuckerStopStep(SuckerStep):
        def __init__(self, pumps):
            super().__init__(pump_instruction(pumps))

    return SuckerStartStep, SuckerStopStep

