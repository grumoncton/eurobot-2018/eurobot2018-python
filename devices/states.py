from enum import IntEnum


class DeviceStates(IntEnum):
    UNINITIATED = -1
    READY = 0
    WORKING = 1
    STOPPED = 2
    DONE = 3

