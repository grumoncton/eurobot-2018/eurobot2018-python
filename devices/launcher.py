from enum import IntEnum

from I2C import I2CInstructions
from .device import Device
from .states import DeviceStates
from task_manager.step import Step


class LauncherInstructions(IntEnum):
    GET_STATE = I2CInstructions.GET_STATE
    SET_SIDE = I2CInstructions.SET_SIDE
    LAUNCH_8 = 0x10
    LAUNCH_4 = 0x11


class LauncherStates(IntEnum):
    UNINITIATED = DeviceStates.UNINITIATED
    READY = DeviceStates.READY
    WORKING = DeviceStates.WORKING
    DONE = DeviceStates.DONE


class Launcher(Device):
    def __init__(self, i2c, addr):
        super().__init__(i2c, addr, LauncherStates)

        self.state = LauncherStates.UNINITIATED


def launcher_init(i2c, launcher):
    launch_4_step = Step(launcher.addr, launcher, LauncherInstructions.LAUNCH_4)
    launch_8_step = Step(launcher.addr, launcher, LauncherInstructions.LAUNCH_8)

    return launch_4_step, launch_8_step

