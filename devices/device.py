from I2C import I2CInstructions


class Device:
    def __init__(self, i2c, addr, States):
        self.i2c = i2c
        self.addr = addr
        self.States = States
        self.state = States.UNINITIATED

    def update_state(self):
        print(self.addr)
        print('Sending update state')
        self.i2c.write(self.addr, I2CInstructions.GET_STATE)
        print('Updating state...')
        self.state = self.States(self.i2c.read(self.addr))
        print('Updated state')
        return self.state

    def is_done(self):
        return self.state == self.States.DONE

    def is_ready(self):
        return self.state == self.States.READY

