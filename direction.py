from enum import IntEnum


class Direction(IntEnum):
    FORWARDS = 0
    BACKWARDS = 1

