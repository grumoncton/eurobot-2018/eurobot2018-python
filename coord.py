import math


class Coord:
    SIZE = 4

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return 'Coord: ({}, {})'.format(self.x, self.y)

    def __add__(self, other):
        return Coord(self.x + other.x, self.y + other.y)

    def __mul__(self, factor):
        return Coord(self.x * factor, self.y * factor)

    def __rmul__(self, factor):
        return self.__mul__(factor)

    def to_json(self):
        return {'x': self.x, 'y': self.y}

    def to_bytes(self, signed=False):
        return list(
            int(self.x * 20).to_bytes(2, byteorder='big', signed=signed) +
            int(self.y * 20).to_bytes(2, byteorder='big', signed=signed)
        )

    @classmethod
    def from_bytes(cls, data, signed=False):

        x = int.from_bytes(data[:2], byteorder='big', signed=signed) / 20
        y = int.from_bytes(data[2:4], byteorder='big', signed=signed) / 20

        return cls(x, y)

    @classmethod
    def from_json(cls, data):
        return cls(data['x'], data['y'])

    @classmethod
    def from_polar(cls, r, a):
        a = math.radians(a)
        return cls(r * math.cos(a), r * math.sin(a))

    @classmethod
    def from_heading(cls, heading):
        return cls(heading.x, heading.y)


class CoordHeading(Coord):
    def __init__(self, x, y, a=None):
        if a is None:
            return super().__init__(x, y)

        super().__init__(x, y)
        self.a = a

    def __str__(self):
        return 'Heading: ({}, {}) {} deg'.format(
            self.x,
            self.y,
            math.degrees(self.a),
        )

    def to_bytes(self):
        return super().to_bytes() + [int(self.a * 255 / (2 * math.pi))]

    @classmethod
    def from_coord(cls, coord, a):
        return cls(coord.x, coord.y, a)

    @classmethod
    def from_bytes(cls, data):
        data = data[:5]
        angle = data.pop()

        return cls.from_coord(
            super().from_bytes(data),
            2 * math.pi * angle / 255,
        )

