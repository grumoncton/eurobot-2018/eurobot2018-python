import time
import threading
from enum import Enum

import bluetooth as bt


class BTInstruction(Enum):
    PING = 'P'
    MATCH_START = 'S'
    SEQUENCE = 'C'


class BluetoothClient:

    """
    Create bluetooth client

    @param uuid: uuid of the host's service
    @param addr: physical address of host bt adapter
    @param callback: Function to call when we recieve data
    """
    def __init__(self, uuid, addr, callback, lcd):
        self.sock = None
        self.uuid = uuid
        self.addr = addr
        self.callback = callback
        self.lcd = lcd

    def find_beacons_service(self, num=1):
        print('Attempting to find service (attempt # {})'.format(num))

        if self.lcd is not None:
            self.lcd.display_string('BT: Attemp #{}'.format(num).ljust(16), line=2)

        services = bt.find_service(uuid=self.uuid, address=self.addr)

        if len(services) == 0:
            time.sleep(5)
            return self.find_beacons_service(num + 1)

        return services[0]

    def find_socket(self):
        service = self.find_beacons_service()
        port = service["port"]
        name = service["name"]
        host = service["host"]

        print("Connecting to \"{}\" on {}".format(name, host))

        if self.lcd is not None:
            self.lcd.display_string('BT: Connected'.ljust(16), line=2)

        # Create the client socket
        sock = bt.BluetoothSocket(bt.RFCOMM)
        sock.connect((host, port))

        self.sock = sock

    def read(self):
        if self.sock is None:
            return None

        return self.sock.recv(1024)

    def send(self, message):
        if self.sock is None:
            return

        self.sock.send(message)

    def begin(self):
        bt_thread = threading.Thread(target=self.loop)
        bt_thread.start()

    def loop(self):
        self.find_socket()

        while True:
            try:
                self.sock.send(BTInstruction.PING.value)
                message = str(self.sock.recv(1024), 'utf-8')
                instruction = BTInstruction(message)

                if instruction is BTInstruction.PING:
                    continue

                response = self.callback(instruction)

                if response:
                    self.sock.send(response)

            except IOError as err:
                print(err)

                if self.lcd is not None:
                    self.lcd.display_string('BT: Disconnected'.ljust(16), line=2)

                self.find_socket()

            #  Ping every 3 seconds
            time.sleep(3)

