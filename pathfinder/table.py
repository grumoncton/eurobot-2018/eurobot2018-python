import os
import math
import json
import itertools
from dotenv import load_dotenv, find_dotenv

from coord import Coord
from utils import timeit
from .graph import Graph

load_dotenv(find_dotenv())


# How close it passes by obstacles
CLEARANCE = 70

OFFSET = round(float(os.getenv('ROBOT_RADIUS'))) + CLEARANCE

# Grid spacing
SPACING = 60

# If the distance between two points is less than this, a link is made
POINT_LINK_SPACE = 100


class Rectangle:
    def __init__(self, points):
        self.points = points

    def is_blocking_point(self, point):
        x = sorted([point.x for point in self.points])
        y = sorted([point.y for point in self.points])

        return (
            point.x > x[0] - OFFSET and
            point.x < x[1] + OFFSET and
            point.y > y[0] - OFFSET and
            point.y < y[1] + OFFSET
        )


class Circle:
    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r

    def is_blocking_point(self, point):
        dist = dist_two_points(self, point)
        return dist < self.r + OFFSET


obstacles = [

    #  Bad water
    Rectangle([Coord(x=1750, y=894), Coord(x=2000, y=2106)]),

    #  Blocks (pretend they're circles)
    Circle(x=540, y=850, r=100),
    Circle(x=540, y=2150, r=100),

    Circle(x=1190, y=300, r=100),
    Circle(x=1190, y=2700, r=100),

    Circle(x=1500, y=1100, r=100),
    Circle(x=1500, y=1900, r=100),
]


def dist_two_points(a, b):
    return math.hypot(a.x - b.x, a.y - b.y)


def point_is_blocked(point):
    for obstacle in obstacles:
        if obstacle.is_blocking_point(point):
            return True

    return False


def generate_point_map():
    end = timeit('Generating point map...')

    points = []

    for x in range(OFFSET, 2000 - OFFSET, SPACING):
        for y in range(OFFSET, 3000 - OFFSET, SPACING):
            point = Coord(x, y)
            if not point_is_blocked(point):
                points.append(point)

    end()

    return points


def regenerate_point_map(points, new_obstacle):
    return [point for point in points
            if not new_obstacle.is_blocking_point(point)]


def calculate_distances(points):
    end = timeit('Calculating distances...')

    edges = []

    #  Get every combination of points without duplicates
    #  range(len(()) is used to get point ids rather than coordinates
    #  Therefor, A and B are point ids (indicies)
    for A, B in itertools.combinations_with_replacement(range(len(points)), 2):
        if A is B:
            continue

        #  Calculate distance between two points
        dist = dist_two_points(points[A], points[B])

        #  If combination valid, add it
        if dist < POINT_LINK_SPACE:
            edges.append((A, B, dist))

    end()

    return edges


def store_table(points, edges):
    end = timeit('Storing table...')

    with open('table.json', 'w') as file:
        json.dump([
            [x.to_json() for x in points],
            edges
        ], file)

    end()


def load_table():
    with open('table.json') as file:
        [points, edges] = json.load(file)
        points = [Coord.from_json(x) for x in points]
        return points, edges


def generate_graph(points, edges):
    end = timeit('Generating graph...')

    graph = Graph()

    graph.nodes = set(range(len(points)))

    for A, B, dist in edges:
        graph.add_edge(A, B, dist)

    end()
    return graph


if __name__ == '__main__':
    points = generate_point_map()
    edges = calculate_distances(points)

    #  print(edges)
    #  store_table(points, edges)

    #  graph = generate_graph(points, edges)

