from collections import defaultdict


class Graph(object):
    def __init__(self):
        self.nodes = set()
        self.edges = defaultdict(list)
        self.distances = {}

    def add_node(self, value):
        self.nodes.add(value)

    def add_edge(self, A, B, distance):
        self.edges[A].append(B)
        self.edges[B].append(A)
        key = (A, B) if A > B else (B, A)
        self.distances[key] = distance

