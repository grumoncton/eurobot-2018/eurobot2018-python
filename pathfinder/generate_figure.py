from matplotlib import pyplot as plt
from .table import obstacles, Circle, Rectangle


def generate_figure(points, path=[]):
    x = []
    y = []
    colours = []

    for point in points:
        x.append(point.x)
        y.append(point.y)
        colours.append('b')

    #  Make path well take red
    for point in path:
        x.append(point.x)
        y.append(point.y)
        colours.append('r')

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(x, y, s=1, c=colours)
    ax.set_aspect('equal')
    ax.margins(0)
    ax.set_xlim([0, 2000])
    ax.set_ylim([0, 3000])

    for obstacle in obstacles:
        shape = None

        if isinstance(obstacle, Circle):
            shape = plt.Circle(
                (obstacle.x, obstacle.y),
                obstacle.r,
                color='k'
            )

        elif isinstance(obstacle, Rectangle):
            a = obstacle.points[0]
            b = obstacle.points[1]
            shape = plt.Rectangle(
                (a.x, a.y),
                b.x - a.x,
                b.y - a.y
            )

        ax.add_artist(shape)

    fig.savefig('figure', dpi=1000)
