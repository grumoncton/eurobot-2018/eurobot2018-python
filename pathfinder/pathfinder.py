import math
import itertools
from collections import deque

from .graph import Graph
from utils import index_of_min_value

AREA_TOLERANCE = 12000


class Pathfinder:

    def __init__(self, points, graph):
        self.points = points
        self.graph = graph

    #  Get the distance between two points
    #  Basically just calculate the hypotenuse
    def dist_two_points(self, a, b):
        return math.hypot(a.x - b.x, a.y - b.y)

    #  Find closest real point to a given point
    #  Real point means a point of the grid
    def find_closest_point(self, A):
        distances = [self.dist_two_points(A, B) for B in self.points]
        index, _ = index_of_min_value(distances)
        return index

    #  Run dijkstra's algorithm using initial as starting point
    def dijkstra(self, initial):
        visited = {initial: 0}
        path = {}

        nodes = set(self.graph.nodes)

        while nodes:
            min_node = None
            for node in nodes:
                if node in visited:
                    if min_node is None:
                        min_node = node
                    elif visited[node] < visited[min_node]:
                        min_node = node
            if min_node is None:
                break

            nodes.remove(min_node)
            current_weight = visited[min_node]

            for edge in self.graph.edges[min_node]:
                key = (min_node, edge) if min_node > edge else (edge, min_node)
                weight = current_weight + self.graph.distances[key]

                if edge not in visited or weight < visited[edge]:
                    visited[edge] = weight
                    path[edge] = min_node

        return visited, path

    """
    Remove "useless" points from a path

    A point is useless if it is directly on the line drawn between the previous
    and the next point

    @param i: Index of point in question
    @param path: Path we're filtering
    """
    def is_point_useful(self, i, path, tolerance):
        def triangle_area(A, B, C):
            return A.x * (B.y - C.y) + B.x * (C.y - A.y) + C.x * (A.y - B.y)

        if i == 0 or i == len(path) - 1:
            return True

        #  Get coordinates of previous, current, and next point
        prev = path[i - 1]
        point = path[i]
        next = path[i + 1]

        #  Get the area of the triangle these three make
        area = triangle_area(prev, point, next)

        #  If the area is 0, the three points are in a straight line

        return abs(area) > tolerance

    """
    Find the shortest path of real points between origin and destination

    This method runs dijkstra's algorithm to find the shortest path using only
    real points between the two points provided. The origin and destination
    points are excluded if they are not real points.

    @param origin: Starting point
    @param destination: Destination point

    @return list of real point (points of the grid) ids (list indices)
    """
    def shortest_path_ids(self, origin, destination):

        #  Get closest real points to origin and destination
        first_point = self.find_closest_point(origin)
        last_point = self.find_closest_point(destination)

        visited, paths = self.dijkstra(first_point)

        #  Walk path starting at destination
        full_path = deque()
        point = paths[last_point]

        while point != first_point:
            full_path.appendleft(point)
            point = paths[point]

        return list(full_path)

    """
    Find the shortest path between origin and destination

    Like shortest_path_ids but it returns a list of coordinate pairs rather
    than ids. Therefore, it we can add the origin and destination even though
    they aren't necessarily real points

    @param origin: Starting point
    @param destination: Destination point

    @return list for coordinates
    """
    def shortest_path(self, origin, destination):

        #  Get shortest path of ids
        path = self.shortest_path_ids(origin, destination)

        points = self.points
        coords = itertools.chain(
            #  Convert ids to coords
            [points[i] for i in path],
            #  Add destination if it's not in path
            [] if destination is points[path[-1]] else [destination],
        )

        coords = list(coords)

        coords = [point for i, point in enumerate(coords)
            if self.is_point_useful(i, coords, 0)]

        coords = [point for i, point in enumerate(coords)
            if self.is_point_useful(i, coords, AREA_TOLERANCE)]

        return coords


if __name__ == '__main__':
    graph = Graph()

    points = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
    for node in points:
        graph.add_node(node)

    graph.add_edge('A', 'B', 10)
    graph.add_edge('A', 'C', 20)
    graph.add_edge('B', 'D', 15)
    graph.add_edge('C', 'D', 30)
    graph.add_edge('B', 'E', 50)
    graph.add_edge('D', 'E', 30)
    graph.add_edge('E', 'F', 5)
    graph.add_edge('F', 'G', 2)

    pathfinder = Pathfinder(points, graph)

    # output: (25, ['A', 'B', 'D'])
    print(pathfinder.shortest_path(graph, 'A', 'D'))

