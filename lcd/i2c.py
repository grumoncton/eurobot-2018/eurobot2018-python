import smbus
import time
from I2C.constants import MINIMUM_TIME_BETWEEN_I2


def force_try(f, *args):
    while True:
        try:
            return f(*args)
        except OSError:
            time.sleep(MINIMUM_TIME_BETWEEN_I2)


class i2c_device:
    def __init__(self, addr, port=1):
        self.addr = addr
        self.bus = smbus.SMBus(port)

    # Write a single command
    def write_cmd(self, cmd):
        force_try(self.bus.write_byte, self.addr, cmd)
        time.sleep(0.0001)

    # Write a command and argument
    def write_cmd_arg(self, cmd, data):
        force_try(self.bus.write_byte_data, self.addr, cmd, data)
        time.sleep(0.0001)

    # Write a block of data
    def write_block_data(self, cmd, data):
        return force_try(self.bus.write_block_data, self.addr, cmd, data)
        time.sleep(0.0001)

    # Read a single byte
    def read(self):
        return force_try(self.bus.read_byte, self.addr)

    # Read
    def read_data(self, cmd):
        return force_try(self.bus.read_byte_data, self.addr, cmd)

    # Read a block of data
    def read_block_data(self, cmd):
        return force_try(self.bus.read_block_data, self.addr, cmd)

