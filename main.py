#!/usr/bin/python3

import sys
from coord import Coord
from dotenv import load_dotenv, find_dotenv
import time
from time import gmtime, strftime
import os

from colour import Colour
from coord import CoordHeading
from device_states import DeviceStates

from task_manager import TaskManager, load_tasks
from task_manager.step import StepGenerator
from stacker import sequence_callback

from I2C import I2C, addrs, I2CInstructions

from ultras import Ultras

from devices.servos import Servos
from devices.wheels import Wheels, WheelStates
from devices.lifter import Lifter
from devices.sucker import Sucker, Pumps
from devices.spinner import Spinner
from devices.launcher import Launcher

from pathfinder import Pathfinder
import pathfinder.table as table
from table_side import TableSide
#  from pathfinder.generate_figure import generate_figure
from get_disconnected_devices import get_disconnected_devices

load_dotenv(find_dotenv())

if os.getenv('CARES_ABOUT_ITS_ULTRAS') == 'true':
    print("Cares")


def main():

    def should_return():
        if not GPIO.input(RESTART_PIN):
            return False

        pushed_time = time.time()

        while GPIO.input(RESTART_PIN):
            if time.time() > pushed_time + 2:
                print("Pushed button for 2 seconds. Restarting")
                return True

            time.sleep(1)

    lcd = None
    #
    #  Setup LCD
    #

    if os.getenv('HAS_LCD') == 'true':
        from lcd import LCD
        lcd = LCD(addrs['lcd'])

    #
    #  Setup GPIO
    #

    GPIO = None
    START_PIN = 20
    SIDE_PIN = 21
    RESTART_PIN = 16

    if os.getenv('HAS_GPIO') == 'true':
        import RPi.GPIO as GPIO

        GPIO.setmode(GPIO.BCM)

        GPIO.setup((START_PIN, SIDE_PIN), GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(RESTART_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    #
    #  Setup bluetooth
    #

    bt_connection = None
    if os.getenv('HAS_BT') == 'true':
        from bt import BTInstruction, BluetoothClient

        def callback(instruction):
            if instruction is BTInstruction.SEQUENCE:
                sequence = bt_connection.read()
                sequence = [Colour(int(x)) for x in sequence]
                print(sequence)
                sequence_callback(sequence)
                return

        print('bluetooth')
        bt_connection = BluetoothClient(
            addr=os.getenv('BT_ADDR'),
            uuid=os.getenv('BT_UUID'),
            callback=callback,
            lcd=lcd,
        )

    #
    #  Generate table
    #

    try:
        points, edges = table.load_table()
    except Exception:
        points = table.generate_point_map()
        edges = table.calculate_distances(points)
        table.store_table(points, edges)

    graph = table.generate_graph(points, edges)

    pathfinder = Pathfinder(points, graph)

    _table_side = None

    #  path = pathfinder.shortest_path(Coord(x=1468, y=2348), Coord.from_polar(r=840, a=30))
    #  for point in path:
        #  print(point)
    #  generate_figure(points, path)
    #  sys.exit()

    #
    #  Setup I2C
    #

    i2c = I2C(interface=int(os.getenv('I2C_INTERFACE')))

    #
    #  Setup classes
    #

    task_manager = TaskManager(i2c, lcd)

    ultras = Ultras(i2c, addrs['ultra'])
    wheels = Wheels(i2c, addrs['wheels'])
    lifter = Lifter(i2c, addrs['lifter'])
    spinner = Spinner(i2c, addrs['spinner'])
    sucker = Sucker(i2c, addrs['sucker'])
    launcher = Launcher(i2c, addrs['launcher'])
    servos = Servos(i2c, addrs['servos'])

    if bt_connection is not None:
        bt_connection.begin()

    #
    #  Wait for GPIO to be pulled
    #

    required_devices = os.getenv('REQUIRED_DEVICES').split(',')
    while True:
        time.sleep(0.1)

        if should_return():
            return

        #  Check for disconnected devices
        disconnected_devices, response = get_disconnected_devices(
            i2c,
            addrs,
            required_devices,
        )

        #  If there are disconnected devices, show them
        if disconnected_devices is not None:
            lcd.display_string(disconnected_devices.ljust(16), line=1)
            continue

        if GPIO is not None:

            #  If connections are good, show table side
            table_side = TableSide(GPIO.input(SIDE_PIN))

            if table_side != _table_side:
                if lcd is not None:
                    lcd.display_string(table_side.name.ljust(16), line=1)

                for device in required_devices:
                    addr = addrs[device]
                    i2c.write_block(addr, I2CInstructions.SET_SIDE, [table_side.value])

                _table_side = table_side

            if not GPIO.input(START_PIN):
                break

    MoveToStep, MoveToPathStep, SuckerStopStep = load_tasks(
        task_manager,
        i2c,
        pathfinder,
        wheels,
        launcher,
        servos,
        lifter,
        spinner,
        sucker,
        _table_side
    )

    #
    #  Match started
    #

    task_manager.start_time = time.time()
    if bt_connection is not None:
        print('Sending: {}'.format(
            BTInstruction.MATCH_START.value +
            str(_table_side.value)
        ))

        bt_connection.send(
            BTInstruction.MATCH_START.value +
            str(_table_side.value)
        )

    if lcd is not None:
        lcd.display_string('Match started'.ljust(16), line=1)

    #
    #  Main loop
    #

    start_delay = os.getenv('START_DELAY')
    print(start_delay)
    if start_delay:
        time.sleep(int(start_delay))

    last_loop_time = time.time()
    TIME_BETWEEN_LOOP = float(os.getenv('TIME_BETWEEN_LOOP'))

    #  while True:
        #  print(ultras.read_distances())
        #  time.sleep(.1)

    #  wheels.pos = CoordHeading(x=500, y=200, a=0)
    #  print(wheels.should_stop([924,  0, 360,   0, 100, 596,   0,  48]))
    #  sys.exit()

    sequence_callback([Colour.BLUE, Colour.YELLOW, Colour.BLACK])

    while True:

        sleep_time = TIME_BETWEEN_LOOP - (time.time() - last_loop_time)
        print(sleep_time)
        if sleep_time > 0:
            time.sleep(sleep_time)

        last_loop_time = time.time()

        if time.time() > task_manager.start_time + 98:
            print("Times up")

            #  Use loop to make sure things get the message
            while True:
                wheels.stop()

                if os.getenv('ROBOT_NAME') == 'builder':
                    SuckerStopStep(Pumps).send()

                time.sleep(1)

        #  If we're blocked and should be moving
        if os.getenv('CARES_ABOUT_ITS_ULTRAS') == 'true' \
           and task_manager.task is not None \
           and (type(task_manager.task.step) is MoveToStep or type(task_manager.task.step) is MoveToPathStep) \
           and ultras.is_blocked() \
           and wheels.should_stop(ultras.distances, _table_side):

            print("\n\nObstacle detected!\n\n")
            print(ultras.distances)

            #  Stop wheels
            wheels.stop()

            #  if had_to_stop:
                #  print("Had to stop", had_to_stop)

            while ultras.is_blocked() and wheels.should_stop(ultras.distances, _table_side):
                print("Obstacle still detected", ultras.distances)
                time.sleep(.1)

            print("Unblocked! ")
                #  time.sleep(.5)

            #  if had_to_stop:

                #  #  Update current position
                #  wheels.update_position()

                #  #  Use block ultra to estimate position of other robot
                #  new_obstacle = ultras.generate_obstacle(wheels.pos)

                #  #  Generate new pathfinding graph using new obstacle
                #  new_points = table.regenerate_point_map(points, new_obstacle)
                #  new_graph = table.generate_graph(new_points, edges)

                #  #  Temporarily set new graph
                #  pathfinder.graph = new_graph

                #  #  Send new path
                #  #  Calling send will automatically generate new path with new points
                #  task_manager.task.step.send()

        #  Tell task manager to perform it's checks
        #  Basically makes sure we have a current task that isn't done
        task_manager.loop()

        #  Update state of current device
        state = task_manager.update_device_state()
        print(state)

        if isinstance(task_manager.task.step.device, Wheels):
            wheels.update_position()
            print(wheels.pos, wheels.current_direction)

        #  print(state)
        #  print(task_manager.task)
        #  print(task_manager.task.step)

        #  Allow step to perform checks
        task_manager.task.step.loop()

        #  If current device done, get finish step
        if task_manager.task.step.device.is_done():
            task_manager.task.new_step()
            task_manager.loop()

        #  if isinstance(task_manager.task.step.device, Wheels):

        #  If device waiting
        if isinstance(task_manager.task.step, StepGenerator) or task_manager.task.step.device.is_ready():
            print(task_manager.task.step)
            task_manager.send_step()


if __name__ == '__main__':
    while True:
        main()
        print("\n\nRestarting...\n\n")

