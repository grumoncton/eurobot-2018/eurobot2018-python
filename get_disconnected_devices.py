from I2C import I2CInstructions
from device_states import DeviceStates


def get_disconnected_devices(i2c, addrs, required_devices):

    for device in required_devices:
        try:
            i2c.bus.read_byte(addrs[device])
        except Exception:
            return '{} disconnected'.format(device), None

    for device in required_devices:
        if device == 'sucker':
            continue

        addr = addrs[device]
        i2c.write(addr, I2CInstructions.GET_STATE)
        response = i2c.read(addr)
        state = None

        try:
            state = DeviceStates(response)
        except Exception:
            print('{} sent a bad device state: {}'.format(device, response))

        if state is not DeviceStates.READY:
            return '{} not ready'.format(device), hex(response)

    return None, None

