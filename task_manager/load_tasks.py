import os
import sys
import time

from .task_manager import Task

from coord import Coord
from table_side import TableSide
from stacker import stacker_init
from devices.servos import servos_init
from devices.lifter import lifter_init
from devices.spinner import spinner_init
from devices.launcher import launcher_init
from devices.sucker import sucker_init, Pumps
from devices.wheel_steps import wheel_init, Direction

BLOCK_PICKUP_DISTANCE = 200


def load_tasks(task_manager, i2c, pathfinder, wheels, launcher, servos, lifter, spinner, sucker, table_side):

    #
    #  Primary robot steps
    #

    launch_4_step, launch_8_step \
        = launcher_init(i2c, launcher)

    close_latch_step, open_latch_step, close_arm_step, open_arm_step, sort_balls_step, unclog_that_ass_step \
        = servos_init(i2c, servos)

    #
    #  Secondary robot steps
    #

    MoveToStep, MoveToRelativeStep, MoveToPathStep, RotateToStep, MoveToWithoutUltraStep, ResetXStep, ResetYStep, ResetAStep \
        = wheel_init(i2c, wheels, pathfinder)

    pickup_height_step, switch_height_step, DropHeightStep, OverHeightStep \
        = lifter_init(i2c, lifter)

    servo_enable_step, servo_disable_step, spinner_cw_step, spinner_ccw_step, servo_down_step \
        = spinner_init(i2c, spinner)

    SuckerStartStep, SuckerStopStep \
        = sucker_init(i2c, sucker)

    Stacker = stacker_init(
        MoveToRelativeStep,
        pickup_height_step,
        DropHeightStep,
        OverHeightStep,
        spinner_cw_step,
        spinner_ccw_step,
        SuckerStartStep,
        SuckerStopStep,
        table_side,
    )

    #  print(Stacker(Task(i2c, 0), Coord(1000, 1000), 0).generate_steps())
    #  sys.exit()

    #  for pump in Pumps:
        #  SuckerStartStep([pump]).send()
        #  time.sleep(5)
        #  SuckerStopStep(Pumps).send()
        #  time.sleep(2)
    #  sys.exit()

    if os.getenv('ROBOT_NAME') == 'builder':

        #  Test task
        #  Task(i2c, steps=[
            #  pickup_height_step,
            #  MoveToStep(Coord(1000, 1000)),
            #  MoveToStep(Coord(496.51, 245.51), Direction.BACKWARDS),
        #  ]),

        #
        #  Get first cubes and do tower
        #

        compensation = Coord(x=-4, y=20)
        first_blocks_position = Coord(x=1190, y=290) + compensation
        SPINNER_CENTER_DISTANCE = 221.2

        first_stack_task = Task(i2c, score=sum(range(5)))
        first_stack_task.steps = [

            #  Get in front of cubes
            MoveToStep(first_blocks_position + Coord(x=0, y=350)),

            #  Go get cubes
            MoveToWithoutUltraStep(first_blocks_position + Coord(x=0, y=SPINNER_CENTER_DISTANCE)),
            RotateToStep(270),

            #  Start all pumps
            SuckerStartStep(Pumps),

            #  Pick up cubes
            pickup_height_step,

            servo_down_step,

            #  Raise cubes off the ground
            DropHeightStep(2),

            servo_disable_step,

            #  Go to construction zone
            MoveToStep(Coord(x=370, y=550), long_way=True),

            #  Stack in order
            Stacker(
                first_stack_task,
                block_rotation=270,
            ),

            SuckerStopStep(Pumps),

            servo_enable_step,

            MoveToStep(Coord(x=540, y=550), Direction.BACKWARDS),

            servo_disable_step,
        ]

        #
        #  Suck cubes and flip switch
        #

        block_compensation = Coord(x=-5, y=5)
        if table_side is TableSide.ORANGE:
            block_compensation += Coord(x=0, y=-20)

        second_blocks_position = Coord(x=540, y=850) + block_compensation

        light_compensation = Coord(x=0, y=0)
        if table_side is TableSide.ORANGE:
            light_compensation += Coord(x=0, y=-20)

        light_position = Coord(x=220, y=1130)

        light_task = Task(i2c, score=25)
        light_task.steps = [

            #  Go to pickup spot
            MoveToStep(second_blocks_position + Coord(x=0, y=-SPINNER_CENTER_DISTANCE)),

            RotateToStep(90),

            #  Start all pumps
            SuckerStartStep(Pumps),

            #  Pick up cubes
            pickup_height_step,
            servo_down_step,

            #  Raise cubes off the ground
            switch_height_step,

            MoveToStep(Coord(x=540, y=light_position.y)),

            MoveToStep(light_position),
            MoveToStep(Coord(x=540, y=light_position.y), Direction.BACKWARDS),

        ]

        second_stack_task = Task(i2c, score=sum(range(5)))
        second_stack_task.steps = [

            #  Go to construction zone
            MoveToStep(Coord(x=370, y=750)),
            RotateToStep(180),

            #  Stack in order
            Stacker(
                second_stack_task,
                block_rotation=270,
            ),

            SuckerStopStep(Pumps),

            servo_enable_step,

            MoveToStep(Coord(x=540, y=550), Direction.BACKWARDS),

            servo_disable_step,
        ]

        task_manager.tasks = [first_stack_task]#, light_task, second_stack_task]

    else:

        #  One-colour balls

        ball_shoot_angle = 5 if table_side is TableSide.ORANGE else -5
        unblock_angle = 12
        one_colour_balls_task = Task(i2c, score=(10 + 5 * 8), steps=[

            #  Grab good balls
            MoveToStep(Coord(x=855, y=200)),

            #  Kobe
            RotateToStep(ball_shoot_angle),
            launch_4_step,

            RotateToStep(unblock_angle),
            RotateToStep(-unblock_angle),
            RotateToStep(ball_shoot_angle),
            launch_4_step,

            RotateToStep(unblock_angle),
            RotateToStep(-unblock_angle),
            RotateToStep(ball_shoot_angle),
            launch_4_step,
        ])

        #  Bee
        bee_position = Coord(x=1830, y=200)
        center_edge_distance = 116.75

        bee_task = Task(i2c, score=50, steps=[

            MoveToStep(bee_position),

            open_arm_step,

            RotateToStep(90),

            close_arm_step,

            #  MoveToStep(bee_position + Coord(x=-300, y=300)),

            MoveToStep(Coord(x=bee_position.x, y=(center_edge_distance - 50)), Direction.BACKWARDS),
            ResetYStep(center_edge_distance),
            ResetAStep(90),
            MoveToRelativeStep(Coord(x=60, y=0)),

            RotateToStep(180),
            MoveToRelativeStep(Coord(x=-80, y=0), Direction.BACKWARDS),
            ResetXStep(2000 - center_edge_distance),
            ResetAStep(180),
            MoveToRelativeStep(Coord(x=60, y=0)),
        ])

        #  Mixed water
        bad_water_thing = Coord(x=1691.11, y=2164.89)
        mixed_colour_balls_task = Task(i2c, score=(10 + 10 * 4 + 5 * 4), steps=[

            #  Get in front of water tower
            MoveToPathStep(Coord(x=1500, y=2395)),

            #  Go to water tower
            MoveToStep(Coord(x=1786, y=2395)),

            #  Get mixed balls
            sort_balls_step,

            MoveToStep(Coord(x=1786, y=2395) + Coord(x=-30, y=0), Direction.BACKWARDS),

            MoveToStep(Coord(x=1786, y=2395) + Coord(x=0, y=-150)),

            #  Get in position to back into bad water thing
            #  MoveToStep(Coord(x=1691.11, y=2164.89) + Coord.from_polar(r=200, a=(90 + 45)), Direction.BACKWARDS),

            #  Back into bad water thing
            MoveToStep(bad_water_thing),

            #  Drop bad water
            open_latch_step,

            #  Get away from bad water
            MoveToStep(Coord(x=1786, y=2395) + Coord(x=-20, y=0), Direction.BACKWARDS),
            #  MoveToStep(bad_water_thing + Coord.from_polar(r=200, a=(90 + 45)), Direction.BACKWARDS),

            close_latch_step,

            #  Get into shooting position
            MoveToPathStep(Coord.from_polar(r=920, a=30)),
            RotateToStep(14),

            #  Kobe
            launch_4_step,
        ])

        task_manager.tasks = [one_colour_balls_task, bee_task, mixed_colour_balls_task]

    return MoveToStep, MoveToPathStep, SuckerStopStep

