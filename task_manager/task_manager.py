import sys
import time

from device_states import DeviceStates
from .step import StepGenerator


class Task:
    def __init__(self, i2c, score, steps=[]):
        self.step = None
        self.steps = steps
        self.score = score

    def is_done(self):
        return not self.steps and self.step is None

    def new_step(self):
        try:
            step = self.steps.pop(0)
        except Exception:
            self.step = None
            print("Out of steps")
            return

        if isinstance(step, StepGenerator):

            #  Get steps from generator
            steps = step.generate_steps()

            #  Next step is first generated step
            step = steps.pop(0)

            #  Add rest of steps to task's steps
            self.steps = steps + self.steps

        self.step = step


class TaskManager:
    def __init__(self, i2c, lcd):
        #  self.destination = None
        self.task = None
        self.tasks = []
        self.i2c = i2c
        self.lcd = lcd

        #  We start with 5 points for the bee OR 5 points for the pannel
        #  Each robot gets each one
        self.score = 5

    def new_task(self):
        print('getting new task')
        if self.task is not None:
            print("Done task. Incrementing score")
            self.score += self.task.score
            self.lcd.display_string('Score: {}'.format(self.score).ljust(16), line=1)

        if len(self.tasks) == 0:
            self.lcd.display_string('Score: {}'.format(self.score).ljust(16), line=1)
            self.lcd.display_string('Done tasks.', line=2)
            print("Done tasks after {} seconds".format(time.time() - self.start_time))
            sys.exit()

        self.task = self.tasks.pop(0)

    def loop(self):
        #  If task not yet set
        if self.task is None:
            self.new_task()

        #  If step not yet set
        if self.task.step is None:
            self.task.new_step()

        #  If task is done, get new task
        if self.task.is_done():
            self.new_task()
            self.task.new_step()

    def send_step(self):
        self.task.step.send()

    def update_device_state(self):
        try:
            return self.task.step.update_device_state()
        except Exception as exp:
            print(exp)
            return DeviceStates.UNINITIATED

