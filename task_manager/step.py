from device_states import DeviceStates


class Step:
    def __str__(self):
        return 'simple_step {} {}'.format(self.device, self.instruction)

    def __init__(self, addr, device, instruction):
        self.addr = addr
        self.device = device
        self.instruction = instruction

    def send(self):
        self.device.i2c.write(self.addr, self.instruction)

    def loop(self):
        pass

    def update_device_state(self):
        if self.addr is None or self.device is None:
            return DeviceStates.UNINITIATED

        return self.device.update_state()


class StepGenerator:
    def generate_steps(self):
        return []

