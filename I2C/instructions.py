from enum import IntEnum


class I2CInstructions(IntEnum):
    GET_STATE = 0x01
    SET_SIDE = 0x02

