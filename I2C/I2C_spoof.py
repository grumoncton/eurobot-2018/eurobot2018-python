from .address_map import addrs


def device_name(addr):
    try:
        return ' ({})'.format(
            list(addrs.keys())[list(addrs.values()).index(addr)]
        )
    except Exception:
        return ''


class I2C_spoof:
    def read_byte(self, addr):
        return int(input('>> i2c reading from {:#04x}{}: '.format(
            addr,
            device_name(addr),
        )))

    def read_i2c_block_data(self, addr):
        print('>> i2c reading block from {:#04x}{}:'.format(
            addr,
            device_name(addr),
        ))
        return [int(input('>> {}: '.format(i))) for i in range(32)]

    def write_byte(self, addr, val):
        print('>> i2c writting to {:#04x}{}: {:#04x}'.format(
            addr,
            device_name(addr),
            val,
        ))

    def write_block(self, addr, cmd, data):
        print('>> i2c writting block to {:#04x}{}:'.format(
            addr,
            device_name(addr),
        ))
        print('>> cmd: {}'.format(cmd))
        print('>> block: {}'.format(data))

