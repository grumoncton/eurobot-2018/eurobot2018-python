import os
import time

from .I2C_spoof import I2C_spoof
from .constants import MINIMUM_TIME_BETWEEN_I2


def force_try(f, *args):
    while True:
        try:
            return f(*args)
        except OSError as err:
            print(err)
            time.sleep(MINIMUM_TIME_BETWEEN_I2)


class I2C:
    def __init__(self, interface):
        if os.getenv('SPOOF_I2C') == 'true':
            self.bus = I2C_spoof()
            return

        from smbus import SMBus
        self.bus = SMBus(interface)

    def read(self, addr):
        return force_try(self.bus.read_byte, addr)

    def read_block(self, addr, cmd):
        return force_try(self.bus.read_i2c_block_data, addr, cmd)

    def write(self, addr, val):
        if val == 0x23:
            print("Sending 0x23 wtf is going on?")
        return force_try(self.bus.write_byte, addr, int(val))

    def write_block(self, addr, cmd, data):
        return force_try(self.bus.write_i2c_block_data, addr, cmd, list(data))

